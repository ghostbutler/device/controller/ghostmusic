package service

import (
	"fmt"
	"github.com/faiface/beep"
	"github.com/faiface/beep/effects"
	"github.com/faiface/beep/mp3"
	"github.com/faiface/beep/speaker"
	"io"
	"os"
)

type PlayerControl struct {
	Streamer beep.StreamSeekCloser
	Control  *beep.Ctrl
	Volume   *effects.Volume
}

type Player struct {
	// name of the sound
	Name string

	// control of the sound
	internal PlayerControl

	// volume
	Volume int

	// finish
	IsFinish bool

	// handler
	handler *Handler
}

// build player
func BuildPlayer(name string,
	storagePath string,
	handler *Handler) (*Player, error) {
	// try to load sound
	if file, err := os.Open(storagePath +
		name); err == nil {
		return BuildPlayerFromStream(file,
			handler,
			name)
	} else {
		return nil, err
	}
}

func BuildPlayerFromStream(reader io.ReadCloser,
	handler *Handler,
	name string) (*Player, error) {
	// allocate
	player := &Player{
		Name:    name,
		handler: handler,
	}

	if streamer, format, err := mp3.Decode(reader); err == nil {
		if format.SampleRate != SampleRate {
			fmt.Println("Warning, sound has sample rate of",
				format.SampleRate,
				"which is different from player one (",
				SampleRate,
				") sound will be different. convert song to player sample rate for normal playback")
		}
		player.internal.Streamer = streamer
		player.internal.Control = &beep.Ctrl{
			Streamer: player.internal.Streamer,
			Paused:   false,
		}
		player.internal.Volume = &effects.Volume{
			Streamer: player.internal.Control,
			Volume:   0,
			Base:     2,
			Silent:   false,
		}
		speaker.Play(beep.Seq(player.internal.Volume, beep.Callback(func() {
			player.IsFinish = true
		})))

		return player, nil
	} else {
		return nil, err
	}
}

// set volume [0-100]
func (player *Player) SetVolume(volume int) {
	speaker.Lock()
	player.internal.Volume.Volume = -1 * (MaximumVolume - (float64(volume)*MaximumVolume)/100)
	player.Volume = volume
	if volume <= 0 {
		player.internal.Volume.Silent = true
	} else {
		player.internal.Volume.Silent = false
	}
	speaker.Unlock()
}

// pause
func (player *Player) Pause() {
	speaker.Lock()
	player.internal.Control.Paused = true
	speaker.Unlock()
}

// play
func (player *Player) Play(isResetPosition bool,
	volume int) {
	speaker.Lock()
	if isResetPosition {
		_ = player.internal.Streamer.Seek(0)
	}
	player.Volume = volume
	player.internal.Volume.Volume = -1 * (MaximumVolume - (float64(volume)*MaximumVolume)/100)
	player.internal.Control.Paused = false
	speaker.Unlock()
}

// stop
func (player *Player) Stop() {
	player.Pause()
	_ = player.internal.Streamer.Close()
}
