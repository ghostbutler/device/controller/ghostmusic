package service

// we do use this library to declare our endpoints
import (
	_ "./statik"
	"github.com/rakyll/statik/fs"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/device"
	"gitlab.com/ghostbutler/tool/service/keystore"
	"gitlab.com/ghostbutler/tool/service/rabbit"
	"net/http"
)

// set here your service version
const (
	VersionMajor = 1
	VersionMinor = 0
)

// this is your service structure, which can contains more then the simple common.Service instance
type Service struct {
	// service
	service *common.Service

	// is running?
	isRunning bool

	// configuration
	configuration *Configuration

	// key manager
	keyManager *keystore.KeyManager

	// music handler
	handler *Handler

	// directory manager
	directory *common.DirectoryManager

	// rabbit mq controller
	rabbitMQController *rabbit.Controller

	// statik file system
	statikFS http.FileSystem
}

// this is the function you will use to build your service
func BuildService(listeningPort int,
	configuration *Configuration) (*Service, error) {
	// allocate service
	service := &Service{
		configuration: configuration,
		isRunning:     true,
		keyManager: keystore.BuildKeyManager(configuration.SecurityManager.Hostname,
			configuration.SecurityManager.Username,
			configuration.SecurityManager.Password,
			common.GhostService[common.ServiceControllerGhostMusic].Name),
	}

	// build directory manager
	service.directory = common.BuildDirectoryManager(configuration.Directory.Hostname,
		service.keyManager)

	// build rabbit mq connection
	if rabbitMQController, err := rabbit.BuildController(configuration.RabbitMQ.Hostname,
		configuration.RabbitMQ.Username,
		configuration.RabbitMQ.Password); err == nil {
		service.rabbitMQController = rabbitMQController
	} else {
		return nil, err
	}
	if _, err := service.rabbitMQController.Channel.QueueDeclare(rabbit.SensorQueueName,
		true,
		false,
		false,
		false,
		nil); err != nil {
		return nil, err
	}

	// create file system
	if f, err := fs.New(); err == nil {
		service.statikFS = f
	} else {
		return nil, err
	}

	// build handler
	if handler, err := BuildHandler(configuration.Storage,
		service.rabbitMQController,
		service.statikFS); err == nil {
		service.handler = handler
	} else {
		return nil, err
	}

	// build service with previous specified data
	service.service = common.BuildService(listeningPort,
		common.ServiceControllerGhostMusic,
		MusicAPIService,
		VersionMajor,
		VersionMinor,
		nil,
		[]device.CapabilityType{device.CapabilityTypeMusic},
		configuration.SecurityManager.Hostname,
		service)

	// service is built
	return service, nil
}

// close service
func (service *Service) Close() {
	service.isRunning = false
	service.rabbitMQController.Close()
}

// is running?
func (service *Service) IsRunning() bool {
	return service.isRunning
}
