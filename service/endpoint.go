package service

import (
	"encoding/json"
	"fmt"
	"gitlab.com/ghostbutler/tool/service"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
)

// define your services ids, must start from last built in index
const (
	// request a music load
	// must provide the source details
	// music is identified by its name
	// will download from http/ftp/sftp sources
	APIServiceV1MusicLoad = common.APIServiceType(iota + common.APIServiceBuiltInLast)

	// delete a loaded music
	// identified by its name
	APIServiceV1MusicDelete

	// share a loaded music
	// identified by its name
	APIServiceV1MusicShare

	// start read of loaded music
	// identified by its name
	APIServiceV1MusicPlay

	// set music volume
	// identified by its name
	APIServiceV1MusicVolume

	// pause music
	// identified by its name
	APIServiceV1MusicPause

	// stop music
	// identified by its name
	APIServiceV1MusicStop

	// list loaded music (already downloaded and ready to be played)
	APIServiceV1MusicList

	// get music player name
	APIServiceV1MusicGetPlayerName

	// set music player name
	APIServiceV1MusicSetPlayerName

	// test player
	APIServiceV1MusicTest
)

var MusicAPIService = map[common.APIServiceType]*common.APIEndpoint{
	APIServiceV1MusicLoad: {
		Path:                    []string{"api", "v1", "controller", "musics", "load"},
		Method:                  "POST",
		Description:             "request song loading (json{name,url[http/https/ftp://],isMustPlay,volume[1-100]})",
		IsMustProvideOneTimeKey: true,
		Callback:                CallbackMusicLoad,
	},
	APIServiceV1MusicDelete: {
		Path:                    []string{"api", "v1", "controller", "musics"},
		Method:                  "DELETE",
		Description:             "request song removal (name)",
		IsMustProvideOneTimeKey: true,
		Callback:                CallbackMusicDelete,
	},
	APIServiceV1MusicShare: {
		Path:                    []string{"api", "v1", "controller", "musics"},
		Method:                  "GET",
		Description:             "share song (name)",
		IsMustProvideOneTimeKey: false,
		Callback:                CallbackMusicShare,
	},

	APIServiceV1MusicPlay: {
		Path:                    []string{"api", "v1", "controller", "musics", "play"},
		Method:                  "PUT",
		Description:             "request song play (name,volume[1-100])",
		IsMustProvideOneTimeKey: true,
		Callback:                CallbackMusicPlay,
	},
	APIServiceV1MusicVolume: {
		Path:                    []string{"api", "v1", "controller", "musics", "volume"},
		Method:                  "PUT",
		Description:             "request song play (name,volume[1-100])",
		IsMustProvideOneTimeKey: true,
		Callback:                CallbackMusicVolume,
	},
	APIServiceV1MusicPause: {
		Path:                    []string{"api", "v1", "controller", "musics", "pause"},
		Method:                  "PUT",
		Description:             "request song pause (name)",
		IsMustProvideOneTimeKey: true,
		Callback:                CallbackMusicPause,
	},
	APIServiceV1MusicStop: {
		Path:                    []string{"api", "v1", "controller", "musics", "stop"},
		Method:                  "PUT",
		Description:             "request song stop (name)",
		IsMustProvideOneTimeKey: true,
		Callback:                CallbackMusicStop,
	},

	APIServiceV1MusicList: {
		Path:                    []string{"api", "v1", "controller", "musics", "list"},
		Method:                  "GET",
		Description:             "request songs list",
		IsMustProvideOneTimeKey: true,
		Callback:                CallbackMusicList,
	},

	APIServiceV1MusicGetPlayerName: {
		Path:                    []string{"api", "v1", "controller", "name"},
		Method:                  "GET",
		Description:             "get player name",
		IsMustProvideOneTimeKey: true,
		Callback:                CallbackGetPlayerName,
	},
	APIServiceV1MusicSetPlayerName: {
		Path:                    []string{"api", "v1", "controller", "name"},
		Method:                  "PUT",
		Description:             "set player name (name)",
		IsMustProvideOneTimeKey: true,
		Callback:                CallbackSetPlayerName,
	},
	APIServiceV1MusicTest: {
		Path:                    []string{"api", "v1", "controller", "musics", "test"},
		Method:                  "PUT",
		Description:             "test player",
		IsMustProvideOneTimeKey: true,
		Callback:                CallbackTestPlayer,
	},
}

// list music
func CallbackMusicList(rw http.ResponseWriter,
	_ *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)

	rw.Header().Add("Content-Type",
		"application/json")
	_, _ = rw.Write(srv.handler.ExportMusicListJson())

	return http.StatusOK
}

// load music
func CallbackMusicLoad(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)

	content, _ := ioutil.ReadAll(request.Body)

	var loadInstruction LoadInstruction
	if err := json.Unmarshal(content,
		&loadInstruction); err == nil {
		if !strings.Contains(string(content),
			"\"volume\"") {
			loadInstruction.Volume = 100
		}
		fmt.Println(loadInstruction.Name, loadInstruction.Url)
		if !IsExtensionAuthorized(loadInstruction.Name) ||
			loadInstruction.Url == "" {
			rw.WriteHeader(http.StatusBadRequest)
			_, _ = rw.Write([]byte("{\"error\":\"empty name/url\""))
			fmt.Println(2)
			return http.StatusBadRequest
		} else {
			if err := srv.handler.DownloadFromSource(&loadInstruction); err != nil {
				rw.WriteHeader(http.StatusInternalServerError)
				_, _ = rw.Write([]byte("{\"error\":\"" +
					err.Error() +
					"\"}"))
				fmt.Println(3)
				return http.StatusInternalServerError
			}
		}
	} else {
		rw.WriteHeader(http.StatusBadRequest)
		_, _ = rw.Write([]byte("{\"error\":\"can't unmarshal\"}"))
		fmt.Println(4)
		return http.StatusBadRequest
	}
	return http.StatusOK
}

// delete music
func CallbackMusicDelete(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)
	if err := request.ParseForm(); err == nil {
		name := common.ExtractFormValue(request,
			"name")
		if name == "" {
			rw.WriteHeader(http.StatusBadRequest)
			_, _ = rw.Write([]byte("{\"error\":\"name is empty\"}"))
			return http.StatusBadRequest
		} else {
			_ = srv.handler.Remove(name)
		}
	} else {
		rw.WriteHeader(http.StatusBadRequest)
		_, _ = rw.Write([]byte("{\"error\":\"can't parse form\"}"))
		return http.StatusBadRequest
	}
	return http.StatusOK
}

// share music
func CallbackMusicShare(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)
	if err := request.ParseForm(); err == nil {
		name := common.ExtractFormValue(request,
			"name")
		if name == "" {
			rw.WriteHeader(http.StatusBadRequest)
			_, _ = rw.Write([]byte("{\"error\":\"name is empty\"}"))
			return http.StatusBadRequest
		} else {
			filePath := srv.configuration.Storage +
				name
			if _, err := os.Stat(filePath); os.IsNotExist(err) {
				rw.Header().Add("Content-Type",
					"application/json")
				rw.WriteHeader(http.StatusBadRequest)
				_, _ = rw.Write([]byte("{\"error\":\"file doesn't exist\"}"))
				return http.StatusBadRequest
			} else {
				splitName := strings.Split(name,
					".")
				if mimeType, ok := AudioTypeMimetype[splitName[len(splitName)-1]]; ok {
					if file, err := os.Open(filePath); err == nil {
						rw.Header().Add("Content-Type",
							mimeType)
						_, _ = io.Copy(rw,
							file)
					} else {
						rw.WriteHeader(http.StatusInternalServerError)
						_, _ = rw.Write([]byte("{\"error\":\"can't open file\"}"))
						return http.StatusInternalServerError
					}
				} else {
					rw.WriteHeader(http.StatusInternalServerError)
					_, _ = rw.Write([]byte("{\"error\":\"can't find mime type\"}"))
					return http.StatusInternalServerError
				}
			}
		}
	} else {
		rw.WriteHeader(http.StatusBadRequest)
		_, _ = rw.Write([]byte("{\"error\":\"can't parse form\"}"))
		return http.StatusBadRequest
	}
	return http.StatusOK
}

// play music
func CallbackMusicPlay(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)
	if err := request.ParseForm(); err == nil {
		name := common.ExtractFormValue(request,
			"name")
		volumeRaw := common.ExtractFormValue(request,
			"volume")
		if name == "" {
			rw.WriteHeader(http.StatusBadRequest)
			_, _ = rw.Write([]byte("{\"error\":\"name is empty\"}"))
			return http.StatusBadRequest
		} else {
			var err2 error
			if volume, err := strconv.Atoi(volumeRaw); err == nil {
				err2 = srv.handler.Play(name,
					false,
					volume)
			} else {
				err2 = srv.handler.Play(name,
					false,
					100)
			}
			if err2 != nil {
				rw.WriteHeader(http.StatusInternalServerError)
				_, _ = rw.Write([]byte("{\"error\":\"" +
					err2.Error() +
					"\"}"))
				return http.StatusInternalServerError
			}
		}
	} else {
		rw.WriteHeader(http.StatusBadRequest)
		_, _ = rw.Write([]byte("{\"error\":\"can't parse form\"}"))
		return http.StatusBadRequest
	}
	return http.StatusOK
}

// change music volume
func CallbackMusicVolume(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)
	if err := request.ParseForm(); err == nil {
		name := common.ExtractFormValue(request,
			"name")
		volumeRaw := common.ExtractFormValue(request,
			"volume")
		if name == "" {
			rw.WriteHeader(http.StatusBadRequest)
			_, _ = rw.Write([]byte("{\"error\":\"name is empty\"}"))
			return http.StatusBadRequest
		} else {
			if volume, err := strconv.Atoi(volumeRaw); err == nil {
				if err = srv.handler.SetVolume(name,
					volume); err != nil {
					rw.WriteHeader(http.StatusInternalServerError)
					_, _ = rw.Write([]byte("{\"error\":\"" +
						err.Error() +
						"\"}"))
					return http.StatusInternalServerError
				}
			} else {
				rw.WriteHeader(http.StatusBadRequest)
				_, _ = rw.Write([]byte("{\"error\":\"volume is not a number\"}"))
				return http.StatusBadRequest
			}
		}
	} else {
		rw.WriteHeader(http.StatusBadRequest)
		_, _ = rw.Write([]byte("{\"error\":\"can't parse form\"}"))
		return http.StatusBadRequest
	}
	return http.StatusOK
}

// pause music
func CallbackMusicPause(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)
	if err := request.ParseForm(); err == nil {
		name := common.ExtractFormValue(request,
			"name")
		if name == "" {
			rw.WriteHeader(http.StatusBadRequest)
			_, _ = rw.Write([]byte("{\"error\":\"name is empty\"}"))
			return http.StatusBadRequest
		} else {
			if err = srv.handler.Pause(name); err != nil {
				rw.WriteHeader(http.StatusInternalServerError)
				_, _ = rw.Write([]byte("{\"error\":\"" +
					err.Error() +
					"\"}"))
				return http.StatusInternalServerError
			}
		}
	} else {
		rw.WriteHeader(http.StatusBadRequest)
		_, _ = rw.Write([]byte("{\"error\":\"can't parse form\"}"))
		return http.StatusBadRequest
	}
	return http.StatusOK
}

// stop music
func CallbackMusicStop(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)
	if err := request.ParseForm(); err == nil {
		name := common.ExtractFormValue(request,
			"name")
		if name == "" {
			rw.WriteHeader(http.StatusBadRequest)
			_, _ = rw.Write([]byte("{\"error\":\"name is empty\"}"))
			return http.StatusBadRequest
		} else {
			if err = srv.handler.Stop(name); err != nil {
				rw.WriteHeader(http.StatusInternalServerError)
				_, _ = rw.Write([]byte("{\"error\":\"" +
					err.Error() +
					"\"}"))
				return http.StatusInternalServerError
			}
		}
	} else {
		rw.WriteHeader(http.StatusBadRequest)
		_, _ = rw.Write([]byte("{\"error\":\"can't parse form\"}"))
		return http.StatusBadRequest
	}
	return http.StatusOK
}

// set player name
func CallbackSetPlayerName(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)
	if err := request.ParseForm(); err == nil {
		name := common.ExtractFormValue(request,
			"name")
		if name == "" {
			rw.WriteHeader(http.StatusBadRequest)
			_, _ = rw.Write([]byte("{\"error\":\"name is empty\"}"))
			return http.StatusBadRequest
		} else {
			if err := srv.handler.SaveName(name); err != nil {
				rw.WriteHeader(http.StatusInternalServerError)
				_, _ = rw.Write([]byte("{\"error\":\"can't save file\"}"))
				return http.StatusInternalServerError
			}
		}
	} else {
		rw.WriteHeader(http.StatusBadRequest)
		_, _ = rw.Write([]byte("{\"error\":\"can't parse form\"}"))
		return http.StatusBadRequest
	}
	return http.StatusOK
}

func CallbackGetPlayerName(rw http.ResponseWriter,
	_ *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)
	rw.Header().Add("Content-Type",
		"application/json")
	_, _ = rw.Write([]byte("[\"" +
		srv.handler.Name +
		"\"]"))
	return http.StatusOK
}

func CallbackTestPlayer(rw http.ResponseWriter,
	_ *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)
	if err := srv.handler.RunTest(); err == nil {
		return http.StatusOK
	} else {
		fmt.Println(err)
		rw.WriteHeader(http.StatusInternalServerError)
		_, _ = rw.Write([]byte("{\"error\":\"can't read test sound: \"" +
			err.Error() +
			"\"}"))
		return http.StatusInternalServerError
	}
}
