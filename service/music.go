package service

import (
	"strings"
)

const (
	SampleRate    = 48000
	BufferSize    = 4800
	MaximumVolume = 7
)

var AuthorizedMusicExtension = []string{
	".mp3",
	".wav",
	".flac",
}

var AudioTypeMimetype = map[string]string{
	"mp3":  "audio/mpeg3",
	"wav":  "audio/wav",
	"flac": "audio/flac",
}

// is extension authorized?
func IsExtensionAuthorized(fileName string) bool {
	for _, extension := range AuthorizedMusicExtension {
		if strings.HasSuffix(strings.ToLower(fileName),
			extension) {
			return true
		}
	}
	return false
}

type LoadInstruction struct {
	// name
	Name string `json:"name"`

	// url
	Url string `json:"url"`

	// is must play after loading?
	IsMustPlay bool `json:"isMustPlay"`

	// volume
	Volume int `json:"volume"`
}

type Music struct {
	// name
	Name string `json:"name"`

	// path
	Path string `json:"path"`
}
