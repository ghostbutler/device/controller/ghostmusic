package service

import (
	"bufio"
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/faiface/beep/speaker"
	"github.com/streadway/amqp"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/device"
	"gitlab.com/ghostbutler/tool/service/rabbit"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"
)

const (
	PlayerNameFileName = "name"
	DefaultPlayerName  = "NameIsUnset"
)

// insecure http client
var InsecureHTTPClient = &http.Client{
	Transport: &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	},
}

type Handler struct {
	storagePath string

	musicList map[string]Music

	sync.Mutex
	player map[string]*Player

	NameMutex sync.Mutex
	Name      string

	isRunning          bool
	rabbitMQController *rabbit.Controller

	statikFileSystem http.FileSystem
}

// build handler
func BuildHandler(storagePath string,
	rabbitMQController *rabbit.Controller,
	statikFileSystem http.FileSystem) (*Handler, error) {
	handler := &Handler{
		isRunning:          true,
		musicList:          make(map[string]Music),
		player:             make(map[string]*Player),
		rabbitMQController: rabbitMQController,
		statikFileSystem:   statikFileSystem,
		storagePath:        storagePath,
	}

	if err := os.MkdirAll(storagePath,
		0755); err != nil {
		return nil, err
	}

	// waiting for boot for alsa to not die like it always do
	time.Sleep(time.Second * 10)

	if err := speaker.Init(SampleRate,
		BufferSize); err != nil {
		return nil, err
	}

	if content, err := ioutil.ReadFile(storagePath +
		PlayerNameFileName); err == nil {
		handler.Name = strings.TrimSpace(string(content))
	}
	if handler.Name == "" {
		handler.Name = DefaultPlayerName
	}

	go handler.updateThread()

	return handler, nil
}

// update music list
func (handler *Handler) updateMusicList() (map[string]Music, error) {
	if fileList, err := ioutil.ReadDir(handler.storagePath); err == nil {
		music := make(map[string]Music)
		for _, file := range fileList {
			if !file.IsDir() &&
				IsExtensionAuthorized(file.Name()) {
				music[file.Name()] = Music{
					Name: file.Name(),
					Path: handler.storagePath + file.Name(),
				}

			}
		}
		return music, nil
	} else {
		return nil, err
	}
}

// publish sensor data
func (handler *Handler) publishSensorData(musicName string,
	url string,
	eventType device.MusicEventType,
	volume int,
	isWarning bool) error {
	if channel := handler.rabbitMQController.Channel; channel != nil {
		err := channel.Publish("",
			rabbit.SensorQueueName,
			false,
			false,
			amqp.Publishing{
				ContentType: "application/json",
				Body: device.DoExport(device.CapabilityTypeMusic,
					device.MusicEvent{
						Volume:    volume,
						MusicName: musicName,
						Name:      handler.Name,
						Event:     eventType,
						IsWarning: isWarning,
						URL:       url,
					}.MarshalJSON()),
			})
		return err
	} else {
		return errors.New("no channel available")
	}
}

// update thread
func (handler *Handler) updateThread() {
	for handler.isRunning {
		if musicList, err := handler.updateMusicList(); err == nil {
			handler.musicList = musicList
		} else {
			fmt.Println(err)
		}
		handler.Lock()
		for name, player := range handler.player {
			if player.IsFinish {
				fmt.Println(name,
					"is finish")
				delete(handler.player,
					name)
			}
		}
		handler.Unlock()
		time.Sleep(time.Second)
	}
}

// export music list to json
func (handler *Handler) ExportMusicListJson() []byte {
	if handler.musicList == nil {
		return nil
	} else {
		list := make([]string,
			0,
			1)
		for _, music := range handler.musicList {
			list = append(list,
				music.Name)
		}
		result, _ := json.Marshal(list)
		return result
	}
}

// download from http/https/ftp source
func (handler *Handler) downloadFromHTTPSource(instruction *LoadInstruction) {
	if request, err := http.NewRequest("GET",
		instruction.Url,
		nil); err == nil {
		if response, err := InsecureHTTPClient.Do(request); err == nil {
			defer common.Close(response.Body)
			if response.StatusCode == http.StatusOK {
				if err == nil {
					if tmpFile, err := ioutil.TempFile(handler.storagePath,
						"tmp"); err == nil {
						writer := bufio.NewWriter(tmpFile)
						if _, err := io.Copy(writer,
							response.Body); err == nil {
							_ = tmpFile.Close()
							_ = os.Rename(tmpFile.Name(),
								handler.storagePath+
									instruction.Name)
							_ = handler.publishSensorData(instruction.Name,
								instruction.Url,
								device.MusicFileEventTypeLoad,
								instruction.Volume,
								false)
							if instruction.IsMustPlay {
								_ = handler.Play(instruction.Name,
									true,
									instruction.Volume)
							}
						} else {
							_ = handler.publishSensorData(instruction.Name,
								instruction.Url,
								device.MusicFileEventTypeLoad,
								instruction.Volume,
								true)
							_ = tmpFile.Close()
							_ = os.Remove(handler.storagePath +
								tmpFile.Name())
							fmt.Println("downloadFromHTTPSource::",
								err)
						}
					} else {
						_ = handler.publishSensorData(instruction.Name,
							instruction.Url,
							device.MusicFileEventTypeLoad,
							instruction.Volume,
							true)
						_, _ = io.Copy(ioutil.Discard,
							response.Body)
						fmt.Println("downloadFromHTTPSource::",
							err)
					}
				} else {
					_ = handler.publishSensorData(instruction.Name,
						instruction.Url,
						device.MusicFileEventTypeLoad,
						instruction.Volume,
						true)
					_, _ = io.Copy(ioutil.Discard,
						response.Body)
					fmt.Println("downloadFromHTTPSource::",
						err)
				}
			} else {
				_ = handler.publishSensorData(instruction.Name,
					instruction.Url,
					device.MusicFileEventTypeLoad,
					instruction.Volume,
					true)
				fmt.Println("downloadFromHTTPSource::",
					response.StatusCode)
			}
		} else {
			_ = handler.publishSensorData(instruction.Name,
				instruction.Url,
				device.MusicFileEventTypeLoad,
				instruction.Volume,
				true)
			fmt.Println("downloadFromHTTPSource::",
				err)
		}
	} else {
		_ = handler.publishSensorData(instruction.Name,
			instruction.Url,
			device.MusicFileEventTypeLoad,
			instruction.Volume,
			true)
		fmt.Println("downloadFromHTTPSource::",
			err)
	}
}

// download sound from source
// returns immediately and run download thread
func (handler *Handler) DownloadFromSource(instruction *LoadInstruction) error {
	switch strings.Split(instruction.Url,
		"://")[0] {
	case "http":
		fallthrough
	case "ftp":
		fallthrough
	case "https":
		go handler.downloadFromHTTPSource(instruction)
		return nil

	default:
		return errors.New("unknown protocol")
	}
}

// play music
func (handler *Handler) Play(name string,
	isForce bool,
	volume int) error {
	handler.Lock()
	defer handler.Unlock()
	if player, ok := handler.player[name]; ok {
		player.Play(false,
			volume)
	} else {
		if _, ok := handler.musicList[name]; ok ||
			isForce {
			if player, err := BuildPlayer(name,
				handler.storagePath,
				handler); err == nil {
				handler.player[name] = player
				player.Play(true,
					volume)
			} else {
				return err
			}
		} else {
			_ = handler.publishSensorData(name,
				"",
				device.MusicFileEventTypePlay,
				volume,
				true)
			return errors.New("unknown music")
		}
	}
	_ = handler.publishSensorData(name,
		"",
		device.MusicFileEventTypePlay,
		volume,
		false)
	return nil
}

// set music volume
func (handler *Handler) SetVolume(name string,
	volume int) error {
	handler.Lock()
	defer handler.Unlock()
	if player, ok := handler.player[name]; ok {
		player.SetVolume(volume)
		_ = handler.publishSensorData(name,
			"",
			device.MusicFileEventTypeVolume,
			volume,
			false)
		return nil
	} else {
		_ = handler.publishSensorData(name,
			"",
			device.MusicFileEventTypeVolume,
			volume,
			true)
		return errors.New("unknown music")
	}
}

// pause music
func (handler *Handler) Pause(name string) error {
	handler.Lock()
	defer handler.Unlock()
	if player, ok := handler.player[name]; ok {
		player.Pause()
		_ = handler.publishSensorData(name,
			"",
			device.MusicFileEventTypePause,
			player.Volume,
			false)
		return nil
	} else {
		_ = handler.publishSensorData(name,
			"",
			device.MusicFileEventTypePause,
			0,
			true)
		return errors.New("unknown music")
	}
}

// stop music
func (handler *Handler) Stop(name string) error {
	handler.Lock()
	defer handler.Unlock()
	if player, ok := handler.player[name]; ok {
		player.Stop()
		delete(handler.player,
			name)
		_ = handler.publishSensorData(name,
			"",
			device.MusicFileEventTypeStop,
			player.Volume,
			false)
		return nil
	} else {
		_ = handler.publishSensorData(name,
			"",
			device.MusicFileEventTypeStop,
			0,
			true)
		return errors.New("unknown music")
	}
}

// remove music
func (handler *Handler) Remove(name string) error {
	_ = handler.Stop(name)
	_ = handler.publishSensorData(name,
		"",
		device.MusicFileEventTypeRemove,
		0,
		false)
	return os.Remove(handler.storagePath +
		name)
}

// set player name
func (handler *Handler) SaveName(name string) error {
	handler.Name = name
	handler.NameMutex.Lock()
	defer handler.NameMutex.Unlock()
	return ioutil.WriteFile(handler.storagePath+
		PlayerNameFileName,
		[]byte(name),
		0664)
}

// run test sound
func (handler *Handler) RunTest() error {
	_ = handler.publishSensorData("test.mp3",
		"",
		device.MusicFileEventTypeTest,
		100,
		false)
	if file, err := handler.statikFileSystem.Open("/test.mp3"); err == nil {
		if player, err := BuildPlayerFromStream(file,
			handler,
			"test"); err == nil {
			handler.Lock()
			defer handler.Unlock()
			if player, ok := handler.player["test"]; ok {
				player.Stop()
				delete(handler.player,
					"test")
			}
			handler.player["test"] = player
			player.Play(true,
				100)
			return nil
		} else {
			return err
		}
	} else {
		return err
	}
}
