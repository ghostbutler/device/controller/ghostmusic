Music player
============

Introduction
------------

Music player is able to download musics from HTTP sources and then to read it.

Important
---------

The sound must have a mp3 with a framerate of 48KB/s.

Installation
------------

On an ubuntu/debian distribution, just run the install.sh script.

When installing on raspbian, ensure you disable the volume option in the configuration file.

Add these lines into install script on raspbian

```bash
export PATH=$PATH:/usr/local/go/bin
export GOPATH=/home/pi/go
```

```bash
sudo apt install libasound2-dev
```

Author
------

SOARES Lucas <lucas.soares.npro@gmail.com>

https://gitlab.com/ghostbutler/device/controller/ghostmusic
